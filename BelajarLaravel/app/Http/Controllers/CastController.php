<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        //cek hasil submit data
        //dd($request->all());
        //validasi Data
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'message' => 'required',
        ]);

        //masukan data inputan request ke table cast di database
        DB::table('cast')->insert([
            'nama' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['message'],
        ]);

        //kita lempar ke halaman /cast
        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        //dd($cast);
        
        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.detail', ['cast' =>$cast]);
    }
    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast' =>$cast]);
    }
    public function update($id, Request $request)
    {

        //Validasi Data
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'message' => 'required',
        ]);

        //Update Data
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['name'],
                    'umur' => $request['umur'],
                    'bio' => $request['message']
                ]
            );

        //Lempar ke URL /cast
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
