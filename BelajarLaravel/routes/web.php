<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-tables', function(){
    return view('page.datatables');
});

//CRUD

//Create Data
//Route untuk mengarah ke tambah cast
Route::get('/cast/create', [CastController::Class, 'create']);
//Route untuk menyimpan data inputan  ke DB table cast
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Route untuk menampilkan semua data yang ada di table cast DB
Route::get('/cast', [CastController::class, 'index']);
//Route untuk ambil detail data berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update Data
//Route untuk mengarah ke form edit cast dengan membawa data berdasarkan id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Route untuk update cast berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update'] );

//Delete Data
//Route untuk hapus data berdasarkan id cast
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);