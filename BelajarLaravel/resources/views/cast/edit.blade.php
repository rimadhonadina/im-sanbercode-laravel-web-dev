@extends('layout.master')

@section('title')
Halaman Tampil Edit Cast
@endsection

@section('content')

<form method="post" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
        <label>Nama :</label>
        <input type="text" value="{{$cast->nama}}" name="name"><br><br>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label>Umur:</label>
        <input type="number" min="10" max="90" value="{{$cast->umur}}" name="umur"><br><br>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <label>Bio :</label><br><br>
        <textarea name="message" rows="10" cols="30">{{$cast->bio}}</textarea>
        <br><br>
        @error('message')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="Submit" name="submit" value="Submit">
    </form>

@endsection