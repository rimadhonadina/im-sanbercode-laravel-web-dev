@extends('layout.master')

@section('title')
Halaman Tampil Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h5>{{$cast->umur}}</h5>
<p>{{$cast->bio}}</p>

@endsection