<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<title>Form Pendaftaraan</title>
<body style="font-family: Times New Roman">
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name :</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
        </select><br><br>
        <label>Language Spoken :</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br><br>
        <input type="checkbox">English<br><br>
        <input type="checkbox">Arabic<br><br>
        <input type="checkbox">Japan<br><br>
        <input type="checkbox">Other<br><br>
        <label>Bio :</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
        <input type="Submit" name="submit" value="Sign Up">

    </form>
</body>
</html>