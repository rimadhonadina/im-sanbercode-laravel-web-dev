TUGAS 10 : SQL

SOAL 1 : Membuat database
create DATABASE myshop;

SOAL 2 : Membuat Table di Dalam Database
create TABLE users(
	id int(8) AUTO_INCREMENT PRIMARY KEY,
	name varchar(255) NOT null,
	email varchar(255) NOT null,
	password varchar(255) NOT null
);

create TABLE categories(
    id int(8) AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT null
);

create TABLE items(
    id int(8) AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT null,
    description varchar(255) ,
    price int(9) NOT null,
    stock int(8) NOT null,
    category_id int(8) NOT null,
    FOREIGN KEY(category_id) REFERENCES categories(id)
);

SOAL 3 : Memasukan Data pada Table
users : 
INSERT INTO users(name, email, password) values ("John Doe", "john@doe.com", "john123");
INSERT INTO users(name, email, password) values ("Jane Doe", "jane@doe.com", "jenita123");

categories :
INSERT INTO categories(name) values ("gadget");
INSERT INTO categories(name) values ("cloth");
INSERT INTO categories(name) values ("men");
INSERT INTO categories(name) values ("women");
INSERT INTO categories(name) values ("branded");

items :
INSERT INTO items(name, description, price, stock, category_id) values ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
INSERT INTO items(name, description, price, stock, category_id) values ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
INSERT INTO items(name, description, price, stock, category_id) values ("IMHO Watch", "jam tangan anak yang jujur banget", 	2000000, 10, 1);

SOAL 4 : Mengambil Data dari Database
a.Mengambil data users
SELECT name, email from users;

b.Mengambil data items
-SELECT * FROM items WHERE price > 1000000;
-SELECT * FROM items WHERE name LIKE "%watch";

c.Menampilkan data items join dengan kategori
SELECT items.*, categories.name FROM items INNER JOIN categories ON items.category_id = categories.id;

SOAL 5 : Mengubah Data dari Database
UPDATE items set price = 2500000 WHERE id = 1;
c.Menampilkan data items join dengan kategori

