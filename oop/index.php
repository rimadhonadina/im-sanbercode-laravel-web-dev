<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("Shaun");
echo "Name : " .$sheep->name. "<br>";
echo "Legs : " .$sheep->legs. "<br>";
echo "Clod Blooded : " .$sheep->coldBlooded. "<br><br>";

$kodok = new Frog("Buduk");
echo "Name : " .$kodok->name. "<br>";
echo "Legs : " .$kodok->legs. "<br>";
echo "Clod Blooded : " .$kodok->coldBlooded. "<br>";
echo $kodok->jump();
echo "<br><br>";

$sungokong = new Ape('Kera Sakti');
echo "Name : " .$sungokong->name. "<br>";
echo "Legs : " .$sungokong->legs. "<br>";
echo "Clod Blooded : " .$sungokong->coldBlooded. "<br>";
echo $sungokong->yell();
echo "<br><br>";


?>